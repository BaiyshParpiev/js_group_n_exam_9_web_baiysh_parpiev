import React from 'react';
import './ContactItem.css';
import Modal from "../UI/Modal/Modal";
import Button from "../UI/Button/Button";

const ContactItem = ({dishes, modal, edit, remove, showModal, close}) => {
    return (
        <div className="DishesItem" onClick={showModal}>
            <img src={dishes.img} alt="Name of dishes"/>
            <h5>Name: {dishes.name}</h5>
            {<Modal show={modal} close={close}>
                <img src={dishes.img} alt="Name of dishes"/>
                <h5>Name: {dishes.name}</h5>
                <h5>phone: {dishes.phone}</h5>
                <h5>Email: {dishes.email}</h5>
                <Button name='Edit' onclick={edit}/>
                <Button name='Remove' onclick={remove}/>
            </Modal>}
        </div>
    );
};

export default ContactItem;