import React from 'react';
import './Navigation.css'
import NavigationItem from "../NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <div className="NavigationItems">
            <NavigationItem to='/' exact>Contacts</NavigationItem>
        </div>
    );
};

export default NavigationItems;