import {
    CHANGE_CONTACT_FAILURE,
    CHANGE_CONTACT_REQUEST, CHANGE_CONTACT_SUCCESS, CONTACT_FAILURE, CONTACT_REQUEST, CONTACT_SUCCESS,
    FETCH_CONTACT_FAILURE,
    FETCH_CONTACT_REQUEST,
    FETCH_CONTACT_SUCCESS, REMOVE_FAILURE, REMOVE_REQUEST, REMOVE_SUCCESS
} from "../actions/EditActions";

const initialState = {
    contact: null,
    loading: false,
    fetchLoading: false,
    error: null,
}

export const editReducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_CONTACT_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_CONTACT_SUCCESS:
            return {...state, fetchLoading: false, contact: action.payload};
        case FETCH_CONTACT_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case CHANGE_CONTACT_REQUEST:
            return {...state, loading: true};
        case CHANGE_CONTACT_SUCCESS:
            return {...state, loading: false,};
        case CHANGE_CONTACT_FAILURE:
            return {...state, loading: false, error: action.payload};
        case CONTACT_REQUEST:
            return {...state, loading: true};
        case CONTACT_SUCCESS:
            return {...state, loading: false,};
        case CONTACT_FAILURE:
            return {...state, loading: false, error: action.payload};
        case REMOVE_REQUEST:
            return {...state, loading: true};
        case REMOVE_SUCCESS:
            return {...state, loading: false,};
        case REMOVE_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}