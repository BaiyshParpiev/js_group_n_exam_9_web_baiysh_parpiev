import {axiosApi} from "../../axiosApi";

export const CONTACT_REQUEST = 'CONTACT_REQUEST';
export const CONTACT_SUCCESS = 'CONTACT_SUCCESS';
export const CONTACT_FAILURE = 'CONTACT_FAILURE';

export const CHANGE_CONTACT_REQUEST = 'CHANGE_CONTACT_REQUEST';
export const CHANGE_CONTACT_SUCCESS = 'CHANGE_CONTACT_SUCCESS';
export const CHANGE_CONTACT_FAILURE = 'CHANGE_CONTACT_FAILURE';


export const FETCH_CONTACT_REQUEST = 'FETCH_CONTACT_REQUEST';
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACT_SUCCESS';
export const FETCH_CONTACT_FAILURE = 'FETCH_CONTACT_FAILURE';

export const REMOVE_REQUEST = 'REMOVE_REQUEST';
export const REMOVE_SUCCESS = 'REMOVE_SUCCESS';
export const REMOVE_FAILURE = 'REMOVE_FAILURE';


export const contactRequest = () => ({type: CONTACT_REQUEST});
export const contactSuccess = () => ({type: CONTACT_SUCCESS});
export const contactFailure = error => ({type: CONTACT_FAILURE, payload: error});

export const removeRequest = () => ({type: REMOVE_REQUEST});
export const removeSuccess = () => ({type: REMOVE_SUCCESS});
export const removeFailure = error => ({type: REMOVE_FAILURE, payload: error});

export const fetchContactRequest = () => ({type: FETCH_CONTACT_REQUEST});
export const fetchContactSuccess = contact => ({type: FETCH_CONTACT_SUCCESS, payload: contact});
export const fetchContactFailure = error => ({type: FETCH_CONTACT_FAILURE, payload: error});

export const changeContactRequest = () => ({type: CHANGE_CONTACT_REQUEST});
export const changeContactSuccess = dishes => ({type: CHANGE_CONTACT_SUCCESS});
export const changeContactFailure = error => ({type: CHANGE_CONTACT_FAILURE, payload: error});

export const fetchContacts = () => {
    return async dispatch => {
        try{
            dispatch(fetchContactRequest());
            const response = await axiosApi.get('/contact.json');
            dispatch(fetchContactSuccess(response.data));
        }catch(error) {
            dispatch(fetchContactFailure(error));
            throw error;
        }
    }
}

export const createContact = dishData => {
    return async dispatch => {
        try{
            dispatch(contactRequest());
            await axiosApi.post('/contact.json', dishData);
            dispatch(contactSuccess());
        }catch(error) {
            dispatch(contactFailure(error));
            throw error;
        }
    }
}

export const changeContact = (id, dishData) => {
    return async dispatch => {
        try{
            dispatch(changeContactRequest());
            await axiosApi.put(`/contact/${id}.json`, dishData);
            dispatch(changeContactSuccess());
        }catch(error) {
            dispatch(changeContactFailure(error));
            throw error;
        }
    }
}

export const removeContact= (id) => {
    return async dispatch => {
        try{
            dispatch(removeRequest());
            await axiosApi.delete(`/contact/${id}.json`);
            dispatch(removeSuccess());
        }catch(error) {
            dispatch(removeFailure(error));
            throw error;
        }
    }
}


