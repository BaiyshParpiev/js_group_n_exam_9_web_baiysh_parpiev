import React from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Edit from "./containers/Contact/Edit";
import EditContact from "./containers/Contact/EditContact/EditContact";

const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={Edit}/>
        <Route path="/contact" component={Edit}/>
        <Route path='/newContact' component={EditContact}/>
        <Route path='/edit/:id' component={EditContact}/>
        <Route render={() => <h1>Not found</h1>}/>
      </Switch>
    </Layout>
);

export default App;
