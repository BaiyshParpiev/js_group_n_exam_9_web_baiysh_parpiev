import React, {useEffect, useState} from 'react';
import './Edit.css';
import Button from "../../components/UI/Button/Button";
import ContactItem from "../../components/DishesItem/ContactItem";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import {fetchContacts, removeContact} from "../../store/actions/EditActions";

const Edit = ({history, match}) => {
    const dispatch = useDispatch();
    const {contact, fetchLoading, error} = useSelector(state => ({
        contact: state.contact.contact,
        fetchLoading: state.contact.fetchLoading,
        error: state.contact.error,
    }), shallowEqual)
    const [modalShow, setModalShow] = useState(false);

    useEffect(() => {
        try {
            const fetchData = async() => {
                await  dispatch(fetchContacts());
            };
            fetchData().then(() => {
                history.replace('/');
            });
        }finally{
            history.replace('/');
        }
    }, [dispatch, modalShow, match.path, history])

    const onHandlerClick = () => {
        history.replace('/newContact');
    }

    const onClickEdit = e => {
        history.replace('/edit/' + e);
        setModalShow(false)
    }

    const onRemove = e => {
        try {
            const fetchData = async() => {
                await dispatch(removeContact(e));
            };
            fetchData().then(() => {
                setModalShow(false)
                history.replace('/contact');
            });
        }finally{
            setModalShow(false);
            history.replace('/contact');
        }
    }

    const showModal = () =>{
        setModalShow(true);
    }
    const purChaseHandler = () => {
        setModalShow(false);
    }

    let dishesComponents = <>
        <div className='container'>
            <div className="row">
                <h2>Edit</h2>
                <Button name='+ Add new Dishes' onclick={onHandlerClick}/>
            </div>
            <div className="column">
                {contact && Object.keys(contact).map(d => {
                    return <ContactItem
                        key={d}
                        dishes={contact[d]}
                        remove={() => onRemove(d)}
                        edit={() => onClickEdit(d)}
                        modal={modalShow}
                        showModal={showModal}
                        close={purChaseHandler}
                    />})}
            </div>

        </div>
    </>;

    if(fetchLoading){
        dishesComponents = <Spinner/>
    }else if(error){
        dishesComponents = <div>Sorry something went wrong</div>
    }


    return  dishesComponents
};

export default Edit;