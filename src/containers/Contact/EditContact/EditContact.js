import React, {useEffect, useState} from 'react';
import Button from "../../../components/UI/Button/Button";
import './EditContact.css';
import {useDispatch, useSelector} from "react-redux";
import {changeContact, createContact} from "../../../store/actions/EditActions";
import {useHistory} from "react-router-dom";
import Spinner from "../../../components/UI/Spinner/Spinner";

const EditContact = ({match}) => {
    const dispatch = useDispatch();
    const contact = useSelector(state => state.contact.contact);
    const loading = useSelector(state => state.contact.loading);
    const error = useSelector(state => state.contact.error);
    const history = useHistory();
    const [info, setInfo] = useState({
        img: '',
        name: '',
        phone: '',
        email: '',
    });

    useEffect(() => {
        if(match.params.id){
            const d = match.params.id;
            setInfo(prev => ({
                ...prev,
                img: contact[d].img,
                name: contact[d].name,
                email: contact[d].email,
                phone: contact[d].phone
            }))
        }
    }, [match.params.id, contact])


    const onChange = e => {
        const {name, value} = e.target;
        setInfo(prev => ({
            ...prev,
            [name]: value,
        }));
    }

    const onHandlerClick = e => {
        e.preventDefault();
        try {
            const fetchData = async() => {
                await  dispatch(createContact(info));
            };
            fetchData().then(() => {
                history.replace('/contact');
            });
        }finally{
            history.replace('/contact');
        }
    };

    const onHandlerEdit = e => {
        e.preventDefault();
        try {
            const fetchData = async() => {
                await  dispatch(changeContact(match.params.id, info));
            };
            fetchData().then(() => {
                history.replace('/contact');
            });
        }finally{
            history.replace('/contact');
        }
    };
    const backToContact = () => {
        history.replace('/contact');
    }

    let edit = (
        <form onSubmit={match.params.id ? onHandlerEdit : onHandlerClick} className='ChangingDishes'>
            <div><input type="text" value={info.name} name='name' onChange={onChange} placeholder='Name:'/></div>
            <div><input type="text" value={info.email} name='email' onChange={onChange} placeholder='Email: '/></div>
            <div><input type="text" value={info.phone} name='phone' onChange={onChange} placeholder='Phone: '/></div>
            <div><input type="text" value={info.img} name='img' onChange={onChange} placeholder='Src to img:'/></div>
            <div><img  src={info.img} name='img' alt='Img preview:'/></div>
            <div><Button onclick={match.params.id ? onHandlerEdit : onHandlerClick} type="submit" name='Save'/> <Button onclick={backToContact} type="button" name='Back to Contacts'/></div>
        </form>
    );

    if(loading){
        edit = <Spinner/>
    }else if(error){
        edit = <div>Sorry something went wrong</div>
    }



    return edit;
};

export default EditContact;