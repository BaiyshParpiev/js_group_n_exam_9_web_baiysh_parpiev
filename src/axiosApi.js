import axios from "axios";

export const axiosApi = axios.create({
    baseURL: 'https://food-project-85dd9-default-rtdb.firebaseio.com/'
})